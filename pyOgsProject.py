"""
This short script aims to allow automatic creation, modification and output of
ogs project files using python scripts
Two classes:
1.) BoundaryCondition: Class to manage boundary conditions in project file
2.) GenerateProject: Class to store all information for the project file.
@date: 2018 - Today
@author: jasper.bathmann@ufz.de
"""


def writeTag(tag_level, tag_name, tag_parameter, file="dummy"):
    string = ""
    for i in range(tag_level):
        string += "    "
    string += '<' + str(tag_name) + '>'
    string += str(tag_parameter)
    string += '</' + str(tag_name) + '>\n'
    if file != "dummy":
        file.write(string)

    return string


class BoundaryCondition:
    def __init__(self, *args):
        if(args[0] == "Neumann" or args[0] == "Dirichlet"):
            self.NormalBoundary(*args)
        elif(args[0] == "NeumannTimeDependant"):
            self.NeumannTimeDependant(*args)
        elif(args[0] == "VariableDependentNeumann"):
            self.VariableDependentNeumann(*args)
        elif(args[0] == "HCNonAdvectiveFreeComponentFlowBoundary"):
            self.NormalBoundary(*args)
        elif(args[0] == "OpenBulkFlowBoundary"):
            self.NormalBoundary(*args)
        else:
            print("Error: unknown boundary condition!")

    def getBoundaryParametersAndMeshName(self):
        return self.mesh, self.parameters

    def NormalBoundary(self, tYpe, meshname, field_name):
        self.type = tYpe
        self.mesh = meshname
        self.field_name = field_name + "_" + self.mesh
        self.parameters = [field_name]

    def VariableDependentNeumann(
            self, tYpe, meshname, constant, prefac1, prefac2, prefac3):
        self.type = tYpe
        self.mesh = meshname
        self.constant = constant + "_" + self.mesh
        self.prefac1 = prefac1 + "_" + self.mesh
        self.prefac2 = prefac2 + "_" + self.mesh
        self.prefac3 = prefac3 + "_" + self.mesh
        self.parameters = [constant, prefac1, prefac2, prefac3]

    def Python(
            self, tYpe, geoset, geometry, bc_object):
        self.type = tYpe
        self.geoset = geoset
        self.geometry = geometry
        self.bc_object = bc_object

    def writeBoundary(self, file):
        file.write('                <boundary_condition>\n')
        writeTag(5, "type", self.type, file)

        if(self.type == "Neumann" or self.type == "Dirichlet" or
           self.type == "HCNonAdvectiveFreeComponentFlowBoundary" or
           self.type == "OpenBulkFlowBoundary"):
            self.writeNormalBoundary(file)
        if(self.type == "VariableDependentNeumann"):
            self.writeVariableDependentNeumannBoundary(file)
        file.write('                </boundary_condition>\n')

    def writeNormalBoundary(self, file):
        writeTag(5, "mesh", self.mesh, file)
        writeTag(5, "parameter", self.field_name, file)

    def writeVariableDependentNeumannBoundary(self, file):
        writeTag(5, "mesh", self.mesh, file)
        writeTag(5, "constant_name", self.constant, file)
        writeTag(5, "coefficient_current_variable_name", self.prefac1, file)
        writeTag(5, "coefficient_other_variable_name", self.prefac2, file)
        writeTag(5, "coefficient_mixed_variables_name", self.prefac3, file)


class GenerateProject:
    def __init__(self, projectname):
        self.name = projectname
        self.boundary_conditions_c, self.boundary_conditions_p = [], []
        self.boundary_parameter = []

    def writeProjectFile(self):
        self.file = open(self.name, "w+")
        self.file.write('<?xml version="1.0" encoding="ISO-8859-1"?>\n')
        self.file.write('<OpenGeoSysProject>\n')
        self.writeMeshAndGeometry()
        self.writeProcessInformation()
        self.writeProcessMedia()
        self.writeTimeLoop()
        self.writeParameters()
        self.writeBoundaryConditions()
        self.writeNonlinearSolvers()
        self.file.write('</OpenGeoSysProject>\n')
        self.file.close()

    def setMesh(self, meshname):
        self.meshname = meshname
        self.meshnames = []
        self.meshnames.append(meshname)

    def writeMeshAndGeometry(self):
        self.file.write('    <meshes>\n')
        for meshname in self.meshnames:
            writeTag(2, "mesh", meshname, self.file)
        self.file.write('    </meshes>\n')

    def setProcessInformation(self, *args):
        self.processname = str(args[0])
        self.processtype = str(args[1])
        self.processvariable_concentration = str(args[2])
        self.processvariable_pressure = str(args[3])
        self.processfluid_density = str(args[4])
        self.processfluid_viscosity = str(args[5])
        self.processmedium_permiablility = str(args[6])
        self.processmedium_porosity = str(args[7])
        self.processmedium_storage = str(args[8])
        self.processfluid_ref_density = str(args[9])
        self.processsol_disper_long = str(args[10])
        self.processsol_disper_tran = str(args[11])
        self.processmol_diff_coeff = str(args[12])
        self.processretard_fac = str(args[13])
        self.processdecay_rate = str(args[14])
        self.processspeci_bo_force = str(args[15])
        self.processsecond_var_type = str(args[16])
        self.processsecond_var_name = str(args[17])
        self.processsecond_var_out_name = str(args[18])

    def setStandardProcessInformation(self):
        self.processname = "hc"
        self.processtype = "ComponentTransport"
        self.process_non_advective = "true"
        self.processvariable_concentration = "concentration"
        self.processvariable_pressure = "pressure"
        self.processfluid_density = "1e3"
        self.processfluid_viscosity = "1.0e-3"
        self.processmedium_permiablility = "kappa1"
        self.processmedium_porosity = "constant_porosity_parameter"
        self.processmedium_storage = "0"
        self.processfluid_ref_density = "rho_fluid"
        self.processretard_fac = "retardation"
        self.processspeci_bo_force = "-9.81"
        self.processsecond_var_type = 'static'
        self.processsecond_var_name = 'darcy_velocity'
        self.processsecond_var_out_name = 'darcy_velocity'
        self.setStandardProcessMedia()

    def setStandardDensityModel(self):
        self.densityModel = "ConcentrationDependent"

    def writeProcessInformation(self):
        self.file.write('    <processes>\n')
        self.file.write('        <process>\n')
        writeTag(2, "name", self.processname, self.file)
        writeTag(2, "type", self.processtype, self.file)
        writeTag(2, "non_advective_form", self.process_non_advective, self.file)
        writeTag(2, "integration_order", 2, self.file)
        self.file.write('            <process_variables>\n')
        writeTag(4, "concentration", self.processvariable_concentration,
                 self.file)
        writeTag(4, "pressure", self.processvariable_pressure, self.file)
        self.file.write('            </process_variables>\n')
        writeTag(3, "specific_body_force", self.processspeci_bo_force,
                 self.file)

        self.file.write('            <secondary_variables>\n')
        long_string = '                <secondary_variable type="'
        long_string += self.processsecond_var_type + '" internal_name="'
        long_string += self.processsecond_var_name + '" output_name="'
        long_string += self.processsecond_var_out_name + '"/>\n'
        self.file.write(long_string)
        self.file.write('            </secondary_variables>\n')
        self.file.write('        </process>\n')
        self.file.write('    </processes>\n')

    def setStandardProcessMedia(self):
        self.media = []
        self.processsol_disper_long = "longitudinal_dispersivity"
        self.processsol_disper_long_value = "1."
        self.processsol_disper_tran = "transversal_dispersivity"
        self.processsol_disper_tran_value = ".2"
        self.processmol_diff_coeff = "molecular_diffusion"
        self.processmol_diff_coeff_value = "1e-9"
        self.appendprocessmedium(0, self.processmol_diff_coeff,
                                 self.processmol_diff_coeff_value,
                                 self.processsol_disper_long,
                                 self.processsol_disper_long_value,
                                 self.processsol_disper_tran,
                                 self.processsol_disper_tran_value,
                                 "retardation_factor",
                                 "1")

    def appendprocessmedium(self, i, diff_name, diff_value,
                            beta_t_name, beta_t_value,
                            beta_l_name, beta_l_value,
                            retard_name, retard_value):

        medium = []
        medium.append('        <medium id="'+str(i)+'">\n')
        medium.append('            <phases>\n')
        medium.append('                <phase>\n')
        medium.append('                    <type>AqueousLiquid</type>\n')
        medium.append('                    <components>\n')
        medium.append('                        <component>\n')
        medium.append(writeTag(7, "name", "concentration"))
        medium.append('                            <properties>\n')
        medium.append('                                <property>\n')
        medium.append(writeTag(9, "name", diff_name))
        medium.append(writeTag(9, "value", diff_value))
        medium.append(writeTag(9, "type", "Constant"))
        medium.append('                                </property>\n')
        medium.append('                                <property>\n')
        medium.append(writeTag(9, "name", retard_name))
        medium.append(writeTag(9, "type", "Constant"))
        medium.append(writeTag(9, "value", retard_value))
        medium.append('                                </property>\n')
        medium.append('                                <property>\n')
        medium.append(writeTag(9, "name", "decay_rate"))
        medium.append(writeTag(9, "type", "Parameter"))
        medium.append(writeTag(9, "parameter_name", "decay"))
        medium.append('                                </property>\n')
        medium.append('                            </properties>\n')
        medium.append('                        </component>\n')
        medium.append('                    </components>\n')

        medium.append('                    <properties>\n')
        medium.append('                        <property>\n')
        medium.append(writeTag(7, "name", "density"))
        if(self.densityModel == "Constant"):
            medium.append(writeTag(7, "type", "Constant"))
            medium.append(writeTag(7, "value", "1000"))
        elif(self.densityModel == "ConcentrationDependent"):
            medium.append(writeTag(7, "type", "Linear"))
            medium.append(writeTag(7, "reference_value",
                                   self.processfluid_density))
            medium.append(
                    '                            <independent_variables>\n')
            medium.append(
                    '                                <independent_variable>\n')
            medium.append(writeTag(9, "variable_name", "concentration"))
            medium.append(writeTag(9, "reference_condition", "0."))

            medium.append(writeTag(9, "slope", "0.701"))
            medium.append(
                    '                                </independent_variable>\n')
            medium.append(
                    '                            </independent_variables>\n')
        elif(self.densityModel == "ConcentrationAndPressureDependent"):
            medium.append(writeTag(7, "type", "ConcentrationAndPressureDependent"))
            medium.append(writeTag(7, "reference_density", self.processfluid_density
                     ))
            medium.append(writeTag(7, "reference_concentration", "0", self.file))
            medium.append(writeTag(7, "fluid_density_concentration_difference_ratio",
                     "5e-10"))
            medium.append(writeTag(7, "reference_pressure", "0"))
            drho_dp = (float(self.processmedium_storage) /
                       float(self.parameter_values[-2]) /
                       float(self.processfluid_density))
            medium.append(writeTag(7, "fluid_density_pressure_difference_ratio",
                     drho_dp))
        medium.append('                        </property>\n')
        medium.append('                        <property>\n')
        medium.append(writeTag(7, "name", "viscosity"))
        medium.append(writeTag(7, "type", "Constant"))
        medium.append(writeTag(7, "value", self.processfluid_viscosity))
        medium.append('                        </property>\n')
        medium.append('                    </properties>\n')
        medium.append('                </phase>\n')
        medium.append('            </phases>\n')
        medium.append('            <properties>\n')
        medium.append('                <property>\n')
        medium.append(writeTag(5, "name", "permeability"))
        medium.append(writeTag(5, "type", "Parameter"))
        medium.append(writeTag(5, "parameter_name", self.processmedium_permiablility))
        medium.append('                </property>\n')
        medium.append('                <property>\n')
        medium.append(writeTag(5, "name", "porosity"))
        medium.append(writeTag(5, "type", "Parameter"))
        medium.append(writeTag(5, "parameter_name", self.processmedium_porosity))
        medium.append('                </property>\n')
        medium.append('                <property>\n')
        medium.append(writeTag(5, "name", beta_l_name))
        medium.append(writeTag(5, "type", "Constant"))
        medium.append(writeTag(5, "value", beta_l_value))
        medium.append('                </property>\n')
        medium.append('                <property>\n')
        medium.append(writeTag(5, "name", beta_t_name))
        medium.append(writeTag(5, "type", "Constant"))
        medium.append(writeTag(5, "value", beta_t_value))
        medium.append('                </property>\n')
        medium.append('            </properties>\n')
        medium.append('        </medium>\n')
        self.media.append(medium)

    def writeProcessMedia(self):
        self.file.write('    <media>\n')
        for medium in self.media:
            for line in medium:
                self.file.write(line)
        self.file.write('    </media>\n')

    def setTimeLoop(self, *args):
        self.process_ref = str(args[0])
        self.nonlinear_solver = str(args[1])
        self.convergence_criterion_type = str(args[2])
        self.convergence_criterion_norm = str(args[3])
        self.convergence_criterion_reltols = str(args[4])
        self.time_disc_type = str(args[5])
        self.outputvariables = args[6]
        self.time_stepping_type = str(args[7])
        self.time_stepping_t_ini = str(args[8])
        self.time_stepping_t_end = str(args[9])
        self.time_stepping_delta_t = str(args[10])
        self.output_type = str(args[11])
        self.output_prefix = str(args[12])
        self.output_each_steps = str(args[13])

    def setStandartFixedTimeLoop(self):
        self.process_ref = "hc"
        self.nonlinear_solver = "basic_picard"
        self.convergence_criterion_type = "PerComponentDeltaX"
        self.convergence_criterion_norm = "NORM2"
        self.convergence_criterion_reltols = "5e-9 5e-9"
        self.time_disc_type = "BackwardEuler"
        self.outputvariables = ["concentration", "pressure", "darcy_velocity"]
        self.time_stepping_type = "FixedTimeStepping"
        self.time_stepping_t_ini = "0.0"
        self.time_stepping_t_end = "1e5"
        self.output_type = "VTK"
        self.output_prefix = "interface_output"
        self.output_each_steps = "1"

    def setStandartAdaptiveTimeLoop(self):
        self.process_ref = "hc"
        self.nonlinear_solver = "basic_picard"
        self.convergence_criterion_type = "PerComponentDeltaX"
        self.convergence_criterion_norm = "NORM2"
        self.convergence_criterion_reltols = "1e-8 1e-8"
        self.time_disc_type = "BackwardEuler"
        self.outputvariables = ["concentration", "pressure", "darcy_velocity"]
        self.time_stepping_type = "IterationNumberBasedTimeStepping"
        self.time_stepping_t_ini = "0.0"
        self.time_stepping_t_end = "1e5"
        self.output_type = "VTK"
        self.output_prefix = "interface_output"
        self.output_each_steps = "1e2"
        self.timeloop_initial_dt = "1e0"
        self.timeloop_minimum_dt = "5e-6"
        self.timeloop_maximum_dt = "10800"#3hrs
        self.timeloop_number_iterations_list = "1 2 3 4 5 6 7 8 9 10 11 12"
        self.timeloop_multiplier_list = " 1.1 1.005 1.002 0.997 0.996 0.995 "
        self.timeloop_multiplier_list += "0.95 0.925 0.85 0.5 0.001 1e-8"
        self.timeloop_fixed_output_times = "432000 864000 1296000 1728000 "
        self.timeloop_fixed_output_times += "2160000 2592000"
        self.timeloop_output_iteration_results = "false"

    def setFixedTimeStepping(self, timerepeats, timedeltaTs):
        self.timeSteppingSettings = []

        if(len(timerepeats) != len(timedeltaTs) != 0):
            error_string = "INPUT ERROR: All input lists for timeloop are "
            error_string += "obliged to have equal length>0!"
            print(error_string)
        else:
            for i in range(len(timerepeats)):
                timestep = []
                timestep.append('                        <pair>\n')
                timestep.append(writeTag(7, "repeat", str(timerepeats[i])))
                timestep.append(writeTag(7, "delta_t", str(timedeltaTs[i])))
                timestep.append('                        </pair>\n')
                self.timeSteppingSettings.append(timestep)

    def setOutputLoops(self, outputrepeats, outputdeltaN):
        self.outputSteppingSettings = []
        if(len(outputrepeats) != len(outputdeltaN) != 0):
            error_string = "INPUT ERROR: All input lists for outputloop are "
            error_string += "obliged to have equal length>0!"
            print(error_string)
            exit()
        else:
            for i in range(len(outputrepeats)):
                outputstep = []
                outputstep.append('                <pair>\n')
                outputstep.append(writeTag(5, "repeat", str(outputrepeats[i])))
                outputstep.append(writeTag(5, "each_steps",
                                           str(outputdeltaN[i])))
                outputstep.append('                </pair>\n')
                self.outputSteppingSettings.append(outputstep)

    def setFixedOutputTimes(self, fixed_output_times):
        self.timeloop_fixed_output_times = fixed_output_times

    def writeTimeLoop(self):
        self.file.write('    <time_loop>\n')
        self.file.write('        <processes>\n')
        self.file.write('            <process ref="'+self.process_ref+'">\n')
        writeTag(4, "nonlinear_solver", self.nonlinear_solver, self.file)

        self.file.write('                <convergence_criterion>\n')
        writeTag(5, "type", self.convergence_criterion_type, self.file)
        writeTag(5, "norm_type", self.convergence_criterion_norm, self.file)
        writeTag(5, "reltols", self.convergence_criterion_reltols, self.file)
        self.file.write('                </convergence_criterion>\n')
        self.file.write('                <time_discretization>\n')
        writeTag(5, "type", self.time_disc_type, self.file)
        self.file.write('                </time_discretization>\n')
        self.file.write('                <time_stepping>\n')
        writeTag(5, "type", self.time_stepping_type, self.file)
        writeTag(5, "t_initial", self.time_stepping_t_ini, self.file)
        writeTag(5, "t_end", self.time_stepping_t_end, self.file)
        if self.time_stepping_type == "FixedTimeStepping":
            self.file.write('                    <timesteps>\n')
            for i in range(len(self.timeSteppingSettings)):

                for j in range(len(self.timeSteppingSettings[i])):
                    self.file.write(self.timeSteppingSettings[i][j])
            self.file.write('                    </timesteps>\n')
        elif self.time_stepping_type == "IterationNumberBasedTimeStepping":
            writeTag(5, "initial_dt", self.timeloop_initial_dt, self.file)
            writeTag(5, "minimum_dt", self.timeloop_minimum_dt, self.file)
            writeTag(5, "maximum_dt", self.timeloop_maximum_dt, self.file)
            writeTag(5, "number_iterations",
                     self.timeloop_number_iterations_list, self.file)
            writeTag(5, "multiplier", self.timeloop_multiplier_list, self.file)

        else:
            print("ERROR: Uknown time stepping type: " +
                  self.time_stepping_type)
            exit()
        self.file.write('                </time_stepping>\n')

        self.file.write('            </process>\n')
        self.file.write('        </processes>\n')
        self.file.write('        <output>\n')
        writeTag(3, "type", self.output_type, self.file)
        writeTag(3, "prefix", self.output_prefix, self.file)
        self.file.write('            <timesteps>\n')
        for i in range(len(self.outputSteppingSettings)):

            for j in range(len(self.outputSteppingSettings[i])):
                self.file.write(self.outputSteppingSettings[i][j])
        self.file.write('            </timesteps>\n')
        if self.time_stepping_type == "IterationNumberBasedTimeStepping":
            self.file.write('            <fixed_output_times>')
            for string in self.timeloop_fixed_output_times:
                self.file.write(' ' + str(string))
            self.file.write(' </fixed_output_times>\n')
            writeTag(3, "output_iteration_results",
                     self.timeloop_output_iteration_results, self.file)
        self.file.write('            <variables>\n')
        for variable in self.outputvariables:
            writeTag(4, "variable", variable, self.file)
        self.file.write('            </variables>\n')
        self.file.write('        </output>\n')
        self.file.write('    </time_loop>\n')

    def setParameters(self, *args):
        n_para = int(len(args)/3)
        self.parameter_names = []
        self.parameter_types = []
        self.parameter_values = []

        for i in range(n_para):
            self.parameter_names.append(args[3*i])
            self.parameter_types.append(args[3*i]+1)
            self.parameter_values.append(args[3*i]+2)

    def setStandartParameters(self):
        retardation_n = "retardation"
        retardation_t = "Constant"
        retardation_v = "1"
        decay_n = "decay"
        decay_t = "Constant"
        decay_v = "0"
        c_ini_n = "c_ini"
        c_ini_t = "MeshNode"
        c_ini_v = "c_ini"
        p_ini_n = "p_ini"
        p_ini_t = "MeshNode"
        p_ini_v = "p_ini"
        constant_porosity_parameter_n = "constant_porosity_parameter"
        constant_porosity_parameter_t = "Constant"
        constant_porosity_parameter_v = "0.5"
        kappa1_n = "kappa1"
        kappa1_t = "Constant"
        kappa1_v = "1.239e-11 0 0 0 1.239e-11 0 0 0 1.239e-11"
        self.parameter_names = [
                retardation_n, decay_n,
                c_ini_n, p_ini_n, constant_porosity_parameter_n, kappa1_n]
        self.parameter_types = [
                retardation_t, decay_t,
                c_ini_t, p_ini_t, constant_porosity_parameter_t, kappa1_t]
        self.parameter_values = [
                retardation_v, decay_v,
                c_ini_v, p_ini_v, constant_porosity_parameter_v, kappa1_v]

    def writeParameters(self):
        self.file.write('    <parameters>\n')
        n_para = len(self.parameter_names)
        for i in range(n_para):
            self.file.write('        <parameter>\n')
            writeTag(3, "name", self.parameter_names[i], self.file)
            writeTag(3, "type", self.parameter_types[i], self.file)
            if self.parameter_types[i] == "MeshNode":
                writeTag(3, "field_name", self.parameter_values[i], self.file)
            if self.parameter_types[i] == "Constant":

                if(len(self.parameter_values[i].split()) > 1):
                    writeTag(3, "values", self.parameter_values[i], self.file)
                else:
                    writeTag(3, "value", self.parameter_values[i], self.file)

            self.file.write('        </parameter>\n')
        for parameter in self.boundary_parameter:
            self.file.write('        <parameter>\n')
            writeTag(3, "name", parameter[0], self.file)
            writeTag(3, "type", "MeshNode", self.file)
            writeTag(3, "field_name", parameter[1], self.file)
            writeTag(3, "mesh", parameter[2], self.file)

            self.file.write('        </parameter>\n')
        self.file.write('    </parameters>\n')

    def resetInitialConditions(self, p_ini, c_ini):
        self.c_ini = c_ini
        self.p_ini = p_ini

    def resetBoundaryConditions(self):
        self.boundary_conditions_c, self.boundary_conditions_p = [], []

    def createBoundaryCondition(self, corresponding_parameter, *args):
        bc = BoundaryCondition(*args)

        if(corresponding_parameter == "c"):
            self.boundary_conditions_c.append(bc)
        if(corresponding_parameter == "p"):
            self.boundary_conditions_p.append(bc)
        mesh, parameters = bc.getBoundaryParametersAndMeshName()
        print(mesh, parameters)
        for parameter in parameters:
            combi = [parameter + "_" + mesh, parameter, mesh]
            if combi not in self.boundary_parameter:
                self.boundary_parameter.append(combi)
        if (bc.mesh+".vtu") not in self.meshnames:
            self.meshnames.append(bc.mesh+".vtu")

    def writeBoundaryConditions(self):
        self.file.write("    <process_variables>\n")
        self.file.write("        <process_variable>\n")
        writeTag(3, "name", self.processvariable_concentration, self.file)
        writeTag(3, "components", "1", self.file)
        writeTag(3, "order", "1", self.file)
        writeTag(3, "initial_condition", self.c_ini, self.file)
        self.file.write("            <boundary_conditions>\n")
        for boundary_condition in self.boundary_conditions_c:
            boundary_condition.writeBoundary(self.file)
        self.file.write("            </boundary_conditions>\n")
        self.file.write("        </process_variable>\n")
        self.file.write("        <process_variable>\n")
        writeTag(3, "name", self.processvariable_pressure, self.file)
        writeTag(3, "components", "1", self.file)
        writeTag(3, "order", "1", self.file)
        writeTag(3, "initial_condition", self.p_ini, self.file)
        self.file.write("            <boundary_conditions>\n")
        for boundary_condition in self.boundary_conditions_p:
            boundary_condition.writeBoundary(self.file)
        self.file.write("            </boundary_conditions>\n")
        self.file.write("        </process_variable>\n")
        self.file.write("    </process_variables>\n")

    def setNonlinearSolvers(self, *args):
        self.solver_name = str(args[0])
        self.solver_type = str(args[1])
        self.solver_max_iter = str(args[2])
        self.solver_linear_solver = str(args[3])
        self.solver_linear_solver_name = str(args[4])
        self.solver_linear_solver_lis = str(args[5])
        self.solver_linear_solver_eigen_type = str(args[6])
        self.solver_linear_solver_eigen_precon = str(args[7])
        self.solver_linear_solver_eigen_max_iter = str(args[8])
        self.solver_linear_solver_eigen_error_tol = str(args[9])
        self.solver_linear_solver_petsc_prefix = str(args[10])
        self.solver_linear_solver_petsc_parameters = str(args[11])

    def setStandardNonlinearSolvers(self):
        self.solver_name = "basic_picard"
        self.solver_type = "Picard"
        self.solver_max_iter = "11"
        self.solver_linear_solver = "general_linear_solver"
        self.solver_linear_solver_name = "general_linear_solver"
        self.solver_linear_solver_lis = "-i bicgstab -p ilut -tol 1e-12 "
        self.solver_linear_solver_lis += "-maxiter 20000"
        self.solver_linear_solver_eigen_type = "BiCGSTAB"
        self.solver_linear_solver_eigen_precon = "DIAGONAL"
        self.solver_linear_solver_eigen_max_iter = "10000"
        self.solver_linear_solver_eigen_error_tol = "1e-12"
        self.solver_linear_solver_petsc_prefix = "hc"
        self.solver_linear_solver_petsc_parameters = "-hc_ksp_type bcgs"
        self.solver_linear_solver_petsc_parameters += " -hc_pc_type bjacobi"
        self.solver_linear_solver_petsc_parameters += " -hc_ksp_rtol 1e-12"
        self.solver_linear_solver_petsc_parameters += " -hc_ksp_max_it 20000"

    def setILUTNonlinearSolvers(self):
        self.solver_name = "basic_picard"
        self.solver_type = "Picard"
        self.solver_max_iter = "300"
        self.solver_linear_solver = "general_linear_solver"
        self.solver_linear_solver_name = "general_linear_solver"
        self.solver_linear_solver_lis = "-i bicgstab -p ilut -tol 1e-10 "
        self.solver_linear_solver_lis += "-maxiter 20000"
        self.solver_linear_solver_eigen_type = "BiCGSTAB"
        self.solver_linear_solver_eigen_precon = "ILUT"
        self.solver_linear_solver_eigen_max_iter = "5000"
        self.solver_linear_solver_eigen_error_tol = "1e-12"
        self.solver_linear_solver_petsc_prefix = "hc"
        self.solver_linear_solver_petsc_parameters = "-hc_ksp_type bcgs"
        self.solver_linear_solver_petsc_parameters += " -hc_pc_type bjacobi"
        self.solver_linear_solver_petsc_parameters += " -hc_ksp_rtol 1e-8"
        self.solver_linear_solver_petsc_parameters += " -hc_ksp_max_it 20000"

    def writeNonlinearSolvers(self):
        self.file.write("    <nonlinear_solvers>\n")
        self.file.write("        <nonlinear_solver>\n")
        writeTag(3, "name", self.solver_name, self.file)
        writeTag(3, "type", self.solver_type, self.file)
        writeTag(3, "max_iter", self.solver_max_iter, self.file)
        writeTag(3, "linear_solver", self.solver_linear_solver, self.file)
        self.file.write("        </nonlinear_solver>\n")
        self.file.write("    </nonlinear_solvers>\n")
        self.file.write("    <linear_solvers>\n")
        self.file.write("        <linear_solver>\n")
        writeTag(3, "name", self.solver_linear_solver_name, self.file)
        writeTag(3, "lis", self.solver_linear_solver_lis, self.file)
        self.file.write("            <eigen>\n")
        writeTag(4, "solver_type", self.solver_linear_solver_eigen_type,
                 self.file)
        writeTag(4, "precon_type", self.solver_linear_solver_eigen_precon,
                 self.file)
        writeTag(4, "max_iteration_step",
                 self.solver_linear_solver_eigen_max_iter, self.file)
        writeTag(4, "error_tolerance",
                 self.solver_linear_solver_eigen_error_tol, self.file)
        self.file.write("            </eigen>\n")
        self.file.write("            <petsc>\n")
        writeTag(4, "prefix", self.solver_linear_solver_petsc_prefix,
                 self.file)
        writeTag(4, "parameters", self.solver_linear_solver_petsc_parameters,
                 self.file)
        self.file.write("            </petsc>\n")
        self.file.write("        </linear_solver>\n")
        self.file.write("    </linear_solvers>\n")
